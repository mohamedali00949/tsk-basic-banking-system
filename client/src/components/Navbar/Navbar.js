import React from 'react';
import {Link, NavLink} from 'react-router-dom'
import { AppBar, Container, Toolbar, Typography } from '@material-ui/core';

import useStyles from './styles';

import logo from './logo.svg';

function Navbar() {
    const classes = useStyles()
  return (
      <AppBar className={classes.appBar} position="sticky">
          <Container maxWidth="lg" style={{display: "contents"}}>
              <Link to="/" className={classes.link}>
                <div className={classes.logo}>
                    <img src={logo} alt="logo" width="40px" height="40px" style={{marginRight: "10px"}} />
                    <Typography>TSF</Typography>
                </div>
              </Link>
            <Toolbar className={classes.toolbar}>
                <Typography component={NavLink} to="/" className={classes.link}>Home</Typography>
                <Typography component={NavLink} to="/customers" className={classes.link}>Customers</Typography>
            </Toolbar>
          </Container>
      </AppBar>
  );
}

export default Navbar;
