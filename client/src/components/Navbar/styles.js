import {makeStyles} from '@material-ui/core/styles';

export default makeStyles((theme) => ({
    appBar: {
        background: "black",
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingInline: '25px',
        [theme.breakpoints.down('sm')]: {
            paddingInline: '15px',
        }
    },
    link: {
        paddingLeft: "5px",
        color: "white",
        textDecoration: "none"
    },
    logo: {
        display: 'flex',
        alignItems: 'center',
        color: 'white',
        justifyContent: 'space-between',
    }
}))