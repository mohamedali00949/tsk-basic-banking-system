import React from 'react';

import {Typography} from '@material-ui/core';

import useStyles from './styles';

function Home() {
  const classes = useStyles()
  return <div className={classes.container}><Typography variant="h1" style={{fontSize: "70px"}}>Welcome to TSK Bank</Typography></div>;
}

export default Home;
