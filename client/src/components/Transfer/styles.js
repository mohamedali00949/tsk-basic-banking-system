import {makeStyles} from '@material-ui/core/styles';

export default makeStyles((theme) => ({
    container: {
        width: '100%',
        height: '100%',
        position: 'absolute',
        top: '0',
        right: '0',
        alignItems: 'center',
        background: '#f0f8ff73',
        zIndex: '1000',
    },
    innerContainer: {
        marginTop: '80px',
        padding: '20px',
        background: "white",
        marginInline: "35px",
        boxShadow: '0 0 4px #ccc',
        textAlign: "center",
        borderRadius: "5px",
    },
    step1 : {
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'space-around',
    },
    buttons : {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-between',
        width: '100%',
        margin: '10px',
    }
}))