import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import useStyles from "./styles";

import Aler from "../Util/Alert";

import {
  Box,
  Stepper,
  Step,
  StepLabel,
  Typography,
  TextField,
  InputAdornment,
  Button,
} from "@material-ui/core";

import { useForm } from "react-hook-form";
import Customers from "../Customers/Customers";

import { getAllCustomers, TRANSFER_MONEY } from "../../actions/customers";

import { addTransfer } from "../../api/index";

import Loading from "../Util/loading";
import { Link } from "react-router-dom";

function Transfer({ setTransferState, setTransfers, transfers, setCustomer }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { customer, customers } = useSelector((state) => state.customers);
  const [error, setError] = useState("");
  const [activeStep, setActiveStep] = useState(0);
  const [isLoading, setIsLoading] = useState(
    useSelector((state) => state.customers.isLoading)
  );
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const [transferData, setTransferData] = useState({
    amount: 0,
    senderId: customer._id,
    reciverId: "",
  });
  const [reciver, setReciver] = useState("");
  const steps = ["select amount", "select customer"];

  const nextStep = () => setActiveStep((prevStep) => prevStep + 1);
  const backStep = () => setActiveStep((prevStep) => prevStep - 1);

  const Confirmation = () => (
    <>
      {isLoading ? (
          <Loading />
      ) : (
        <>
          <Typography
            variant="h2"
            style={{ textAlign: "center", fontSize: "45px", fontWeight: "400" }}
          >
            Succeed Transfer
          </Typography>
          <Typography>Thanks</Typography>
          <div className={classes.buttons}>
            <Button onClick={() => setTransferState(false)} color="primary" variant="outlined">Back to customer</Button>
            <Button component={Link} to="/customers" variant="contained" color="primary" style={{marginBlock: "10px"}}>View customers</Button>
          </div>
        </>
      )}
    </>
  );

  const handleSubmitAmount = async (data) => {
    console.log(data);
    await setTransferData({ ...transferData, amount: Number(data.amount) });
    if (customers.length === 0) {
      await dispatch(getAllCustomers(1, setError));
    }
    await nextStep();
  };

  const Step1 = ({ customer }) => {
    return (
      <form
        className={classes.step1}
        onSubmit={handleSubmit(handleSubmitAmount)}
      >
        {errors.amount && (
          <Aler
            message={errors.amount.message}
            type="error"
            title="Error"
            className="authError"
            style={{ top: "80px" }}
          />
        )}
        <Typography variant="h2" style={{ textTransform: "capitalize" }}>
          select amount
        </Typography>
        <div
          style={{ width: "100%", marginBlock: "10px", paddingBlock: "5px" }}
        >
          <TextField
            {...register("amount", {
              required: "The amount is required",
              max: {
                value: customer.balance,
                message: "The amount is larg more balance customer",
              },
              min: { value: 1, message: "The amount must be larger than 0." },
            })}
            label="Amount"
            variant="outlined"
            type="number"
            InputProps={{
              startAdornment: (
                <InputAdornment position="start">$</InputAdornment>
              ),
            }}
          />
        </div>
        <div className={classes.buttons}>
          <Button
            type="button"
            variant="outlined"
            color="primary"
            style={{ textTransform: "capitalize" }}
            onClick={() => setTransferState((prev) => !prev)}
          >
            back
          </Button>
          <Button
            type="submit"
            variant="contained"
            color="primary"
            style={{ textTransform: "capitalize" }}
          >
            next
          </Button>
        </div>
      </form>
    );
  };

  const handleSubmitTransfer = async (e) => {
     await e.preventDefault();
    await setIsLoading(true);
    const data = Object.assign(transferData, { reciverId: reciver });
    await addTransfer(data)
      .then(({data}) => {
        setTransfers((prev) => [...prev, data]);
        dispatch({ type: TRANSFER_MONEY, data: data});
        setCustomer(customer);
        console.log(data);
        nextStep();
      })
      .catch((error) => {
        setError(error?.response?.data?.message || 'Something went wrong at transfer.');
        console.log(error.response, data);
        console.error(error)
      });
    await setIsLoading(false);
  };

  const Step2 = () => {
    return (
      <form onSubmit={handleSubmitTransfer}>
        <Customers
          reciver={reciver}
          setReciver={setReciver}
          transfer={true}
          sender={customer._id}
        />
        <div className={classes.buttons}>
          <Button
            type="button"
            variant="outlined"
            color="primary"
            style={{ textTransform: "capitalize" }}
            onClick={() => backStep()}
          >
            back
          </Button>
          <Button
            type="submit"
            variant="contained"
            color="primary"
            style={{ textTransform: "capitalize" }}
            disabled={!reciver || reciver === customer._id}
          >
            next
          </Button>
        </div>
      </form>
    );
  };

  const Form = () =>
    activeStep === 0 ? <Step1 customer={customer} /> : <Step2 />;

  return (
    <div className={classes.container} style={{position: (activeStep !== 1 && transfers.length >= customers.length) && "fixed", height: "100%"}}>
      {error && (
        <Aler
          message={error}
          type="error"
          title="Error"
          className="authError"
          style={{ top: "80px" }}
        />
      )}
      <div className={classes.innerContainer}>
        <Box sx={{ width: "100%" }}>
          <Stepper activeStep={activeStep} alternativeLabel>
            {steps.map((label) => (
              <Step key={label}>
                <StepLabel style={{ textTransform: "capitalize" }}>
                  {label}
                </StepLabel>
              </Step>
            ))}
          </Stepper>
          {isLoading ? <Loading /> : (
              <>
                {activeStep === steps.length ? <Confirmation /> : <Form />}
              </>
          )}
        </Box>
      </div>
    </div>
  );
}

export default Transfer;
