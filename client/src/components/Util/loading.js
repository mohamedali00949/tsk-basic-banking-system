import React from 'react';
import { CircularProgress, Paper } from "@material-ui/core";

import useStyles from './styles';

function Loading() {
  const classes = useStyles();
  return (
    <Paper elevation={0} className={classes.isLoading}>
    <CircularProgress size="7em" />
  </Paper>
  );
}

export default Loading;
