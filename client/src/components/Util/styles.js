import {makeStyles} from '@material-ui/core/styles';

export default makeStyles((theme) => ({
    isLoading: {
        backgroundColor: "transparent",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        padding: "20px",
        borderRadius: "15px",
        height: "39vh",
        boxShadow: "none",
    },
}))