import { makeStyles } from '@material-ui/core/styles';

export default makeStyles((theme) => ({
    container: {
        marginBlock: '50px',
        paddingInline: '20px',
    },
    balance: {
        background: 'white',
        paddingInline: '20px',
        paddingBlock: '20px',
        marginBlock: '10px',
        borderRadius: '20px',
        boxShadow: '0px 0px 4px 0px #ccc',
        textAlign: 'center',
    },
    balanceContainer: {
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'space-between',
    },
    transfer: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-start',
        marginBlock: '10px',
        paddingBlock: '10px',
        border: '0',
        borderBottom: '2px solid #ccc',
        borderStyle: 'dotted',
        textAlign: 'initial',
        marginInline: '20px',
    },
    transferIcon: {
        marginRight: '30px',
        background: '#aca69e',
        padding: '5px',
        borderRadius: '50%',
        width: '55px',
        height: '55px',
    }
}))