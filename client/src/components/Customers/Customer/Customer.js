import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { useParams } from "react-router-dom";

import { Button, Grid, Typography } from "@material-ui/core";
import useStyles from "./styles";
import money from "./money.svg";
import transfer from "./transfer.svg";

import {ACTIVE_CUSTOMER} from '../../../actions/customers';
import { getOne, getUserTransfer } from "../../../api/index";

import Loading from "../../Util/loading";
import Aler from "../../Util/Alert";
import Transfer from "../../Transfer/Transfer";

function Customer() {
  const { id } = useParams();
  const classes = useStyles();
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const activeCustomer = useSelector((state) => state.customers?.customer);
  const [customer, setCustomer] = useState(
    useSelector((state) =>
      state.customers.customers.find((c) => c._id === id)
    ) || {}
  );
  const [error, setError] = useState('');
  const [transferState, setTransferState] = useState(false);
  const [transfers, setTransfers] = useState([]);
  const [transfersData, setTransfersData] = useState({total: undefined});
  const [transLoading, setTransLoading] = useState(true);

  useEffect(() => {
    if (!customer._id || !activeCustomer._id) {
      setLoading(true);
      getOne(id)
        .then((results) => {
          setCustomer(results.data);
          console.log(results.data);
        })
        .catch((error) => {
          console.error(error);
          setError(error?.response.data.message)
        });
      setLoading(false);
    }
  }, [id]); // eslint-disable-line

  useEffect(() => {
    setTransLoading(true);
    if (transfers?.data?.length === 0 || transfers.total === undefined) {
      getUserTransfer(id, 1).then((results) => {
        setTransfers(results.data.data);
        setTransfersData({total: results.data.total, numberOfPages: results.data.numberOfPages, currentPage: results.data.currentPage})
        console.log(results.data);
      });
    }
    setTransLoading(false);
  }, []); // eslint-disable-line

  const showMore = () => {
    getUserTransfer(id, transfersData?.currentPage + 1).then((results) => {
      setTransfers((prev) => prev.concat(results.data.data));
      setTransfersData({total: results.data.total, numberOfPages: results.data.numberOfPages, currentPage: results.data.currentPage})
      console.log(results.data);
    }).catch((error) => {setError(error?.response.data.message)});
  }

  const sendMoney = () => {
    dispatch({type: ACTIVE_CUSTOMER, data: customer});
    setTransferState(true);
  }

  if (loading || !customer._id || transLoading) {
    return <Loading />;
  }

  if (transferState) {
    return <Transfer setTransferState={setTransferState} setTransfers={setTransfers} transfers={transfers} setCustomer={setCustomer} />
  }

  const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };

  return (
    <div className={classes.container}>
      {error && (
          <Aler
            message={error}
            type="error"
            title="Error"
            className="authError"
            style={{ top: "80px" }}
          />
        )}
      <Typography variant="h2" style={{ textTransform: "capitalize" }}>
        {customer.name}
      </Typography>
      <Grid container>
        <Grid item xs={12} sm={12} md={6} style={{ paddingInline: 10 }}>
          <div className={classes.balance}>
            <div className={classes.balanceContainer}>
              <div
                style={{
                  display: "flex",
                  alignItems: "center",
                  width: "100%",
                  justifyContent: "space-between",
                }}
              >
                <img src={money} alt="money" style={{ width: "45px" }} />
                <Typography variant="h4">
                  Balance: {customer.balance} USD
                </Typography>
              </div>
              <Button
                variant="outlined"
                color="primary"
                style={{
                  marginTop: 10,
                  borderRadius: "25px",
                  textTransform: "capitalize",
                }}
                onClick={() => sendMoney()}
              >
                Transfer Money
              </Button>
            </div>
          </div>
        </Grid>
        <Grid
          item
          xs={12}
          sm={12}
          md={6}
          style={{ padding: 10, textAlign: "center" }}
        >
          <Typography variant="h4" align="center">
            Transfers
          </Typography>
          {(transLoading && transfers?.length === 0) ? (
            <Loading />
          ) : (
            <>
              {transfers?.length === 0 ? (
                <Typography>No transfers</Typography>
              ) : (
                <>
                  {transfers?.map((tr, index) => (
                    <div key={tr?._id} className={classes.transfer} style={{borderBottom: transfers.length === index + 1 && '0px'}}>
                      <div className={classes.transferIcon}><img src={transfer} alt="transfer" style={{ width: "45px" }} /></div>
                      <div>
                        <Typography variant="h5" >{`${tr.senderId === id ? tr.reciverName : tr.senderName}`} - {tr.amount} USD</Typography>
                        <Typography style={{color: "#687173"}}>{(new Date(tr.createdAt)).toLocaleDateString(undefined, options)}</Typography>
                        <Typography style={{color: "#687173"}}>{tr.senderId === id ? 'Send' : "Recive"}</Typography>
                      </div>
                    </div>
                  ))}
                  {(transfersData?.numberOfPages > transfersData?.currentPage || transfersData?.total > transfers.length) && (
                    <Button
                      variant="outlined"
                      color="primary"
                      style={{
                        marginTop: 10,
                        borderRadius: "25px",
                        textTransform: "capitalize",
                      }}
                      disabled={transfersData?.numberOfPages === transfersData?.currentPage}
                      onClick={() => showMore()}
                    >
                      show more
                    </Button>
                  )}
                </>
              )}
            </>
          )}
        </Grid>
      </Grid>
    </div>
  );
}

export default Customer;
