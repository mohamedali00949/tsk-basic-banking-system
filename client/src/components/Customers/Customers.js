import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";

import { getAllCustomers } from "../../actions/customers";
import Loading from "../Util/loading";
import Aler from "../Util/Alert";

import { Button, Typography } from "@material-ui/core";
import useStyles from "./styles";

function Customers({ transfer, reciver, setReciver, sender }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [activeCustomer, setActiveCustomer] = useState("");
  const [error, setError] = useState("");
  const { customers, isLoading, currentPage, numberOfPages, total } =
    useSelector((state) => state.customers);

  useEffect(() => {
    if (customers.length === 0) {
      dispatch(getAllCustomers(1, setError));
    }
  }, []); // eslint-disable-line

  if (isLoading && customers.length === 0) {
    return <Loading />;
  }

  const moreCustomers = () => {
    dispatch(getAllCustomers(Number(currentPage) + 1, setError));
  };

  return (
    <div style={{ marginBlock: "40px", marginInline: "25px" }}>
      {error && (
        <Aler
          message={error}
          type="error"
          title="Error"
          className="authError"
        />
      )}
      <div className={classes.container}>
        <div className={classes.head}>
          <Typography variant="h2">Customers</Typography>
        </div>
        <table className={classes.table}>
          <thead>
            <tr
              className={classes.row}
              style={{ borderTop: "1px solid rgba(224, 224, 224, 1)" }}
            >
              {transfer && <th className={classes.cell}></th>}
              {!transfer && (<th className={classes.cell}>Id</th>)}
              {!transfer && <th className={classes.cell}>Show</th>}
              <th className={classes.cell}>Name</th>
              <th className={classes.cell}>Email</th>
              <th className={classes.cell}>Phone</th>
              <th className={classes.cell}>Balance</th>
              <th className={classes.cell} style={{ borderRight: "0" }}>
                Start
              </th>
            </tr>
          </thead>
          <tbody>
            {customers.filter(c => c._id !== sender).map((customer) => (
              <tr
                key={customer._id}
                className={classes.row}
                onClick={() => {
                    setActiveCustomer((prev) =>
                      prev === customer._id ? "" : customer._id
                    )
                    if (transfer && customer._id !== sender) setReciver(customer._id);
                  }
                }
                style={{
                  background:
                    (activeCustomer === customer._id || reciver === customer._id)  && "rgb(231 243 255)",
                }}
              >
                {transfer && (
                  <td className={classes.cell}>
                    <input
                      type="radio"
                      checked={customer._id === reciver}
                      disabled={customer._id === sender}
                      onChange={() => {if (transfer && customer._id !== sender) setReciver(customer._id)}}
                    />
                  </td>
                )}
                {!transfer && (<td className={classes.cell}>{customer._id}</td>)}
                {!transfer && (
                  <td className={classes.cell}>
                    <Button
                      variant="contained"
                      color="primary"
                      component={Link}
                      to={`/customers/${customer._id}`}
                      style={{ textTransform: "capitalize" }}
                    >
                      Show
                    </Button>
                  </td>
                )}
                <td className={classes.cell}>{customer.name}</td>
                <td className={classes.cell}>{customer.email}</td>
                <td className={classes.cell}>{customer.phone}</td>
                <td className={classes.cell}>{customer.balance}</td>
                <td className={classes.cell} style={{ borderRight: "0" }}>
                  {customer.createdAt}
                </td>
              </tr>
            ))}
          </tbody>
        </table>
        {isLoading && <Loading />}
        {!isLoading && (
          <div className={classes.Footer}>
            <Button
              variant="contained"
              color="primary"
              disabled={currentPage === numberOfPages}
              onClick={() => moreCustomers()}
            >
              Load More
            </Button>
            <Typography>
              {customers.length} of {total}
            </Typography>
          </div>
        )}
      </div>
    </div>
  );
}

export default Customers;
