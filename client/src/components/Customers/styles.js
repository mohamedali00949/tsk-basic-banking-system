import { makeStyles } from "@material-ui/core/styles";

export default makeStyles((theme) => ({
  container: {
    border: "1px solid rgba(224, 224, 224, 1)",
    marginBlock: "10px",
    borderRadius: "4px",
    overflow: "auto",
    background: "#fff",
  },
  head: {
    display: "flex",
    flexDirection: "row",
    padding: "10px",
    justifyContent: "center",
  },
  table: {
    width: "100%",
    borderCollapse: "collapse",
    color: "rgba(0, 0, 0, 0.87)",
    fontFamily: '"Roboto","Helvetica","Arial",sans-serif',
    fontWeight: "400",
    fontSize: "0.875rem",
    lineHeight: "1.43",
    letterSpacing: "0.01071em",
    marginBlock: "14px",
    textAlign: "left",
  },
  row: {
    borderBottom: "1px solid rgba(224, 224, 224, 1)",
  },
  cell: {
    padding: "10px 16px",
    borderRight: "1px solid #e4e4e4",
  },
  Footer: {
    display: 'flex',
    justifyContent: 'space-between',
    marginBlock: 10,
    paddingInline: '10px', 
    borderRadius: "5px"
  },
}));
