import React from 'react';

import { BrowserRouter, Route, Routes } from "react-router-dom";

import { Container } from "@material-ui/core";

import Navbar from './components/Navbar/Navbar';
import Home from './components/Home/Home';
import Customers from './components/Customers/Customers';
import Customer from './components/Customers/Customer/Customer';

function App() {
  return (
    <BrowserRouter>
        <Navbar />
        <Container maxWidth="xl">
          <Routes>
              <Route path="/" exact element={<Home />} />
              <Route path="/customers/:id" exact element={<Customer />} />
              <Route path="/customers" exact element={<Customers />} />
          </Routes>
        </Container>
      </BrowserRouter>
  );
}

export default App;
