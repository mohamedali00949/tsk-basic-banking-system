import * as api from '../api/index';

export const START_LOADING = 'START_LOADING';
export const END_LOADING = 'END_LOADING';

export const GET_ALL_CUSTOMERS = 'GET_ALL_CUSTOMERS';
export const ACTIVE_CUSTOMER = 'ACTIVE_CUSTOMER';
export const TRANSFER_MONEY = 'TRANSFER_MONEY';

export const getAllCustomers = (page, setError) => async dispatch => {
    try {
        dispatch({ type: START_LOADING });

        const {data} = await api.getAllCustomers(page);

        dispatch({ type: GET_ALL_CUSTOMERS, payload: {data : data.data, currentPage: data.currentPage, numberOfPages: data.numOfPages, total: data.total} })

        dispatch({ type: END_LOADING });
    } catch (error) {
        dispatch({ type: END_LOADING });
        setError(error.response?.data?.message || 'Something went wrong at')
    }
}