import {
  START_LOADING,
  END_LOADING,
  GET_ALL_CUSTOMERS,
  ACTIVE_CUSTOMER,
  TRANSFER_MONEY,
} from "../actions/customers";

const customers = (
  state = { isLoading: false, customers: [], customer: {} },
  action
) => {
  switch (action.type) {
    case START_LOADING:
      return { ...state, isLoading: true };
    case END_LOADING:
      return { ...state, isLoading: false };
    case GET_ALL_CUSTOMERS: //data : data.data, currentPage: data.currentPage, numberOfPages: data.numberOfPages, total: data.total
      return {
        ...state,
        customers: state.customers.concat(action.payload.data),
        total: action.payload.total,
        currentPage: action.payload.currentPage,
        numberOfPages: action.payload.numberOfPages,
      };
    case ACTIVE_CUSTOMER:
      return { ...state, customer: action.data };
    case TRANSFER_MONEY:
      let after = state.customers.map((c) => {
        if (c._id === action.data.senderId) {
          return Object.assign(c, { balance: c.balance - action.data.amount });
        } else if (c._id === action.data.reciverId) {
          return Object.assign(c, { balance: c.balance + action.data.amount })
        } else {
            return c;
        }
      })

      return {
        ...state,
        customers: after,
        customer: {...state.customer, balance: state.customer.balance - action.data.amount}
      };
    default:
      return state;
  }
};

export default customers;
