import axios from 'axios';

const API = axios.create({ baseURL: "https://tsk-basic-banking-system.herokuapp.com" });

// Customers API methods 

export const getAllCustomers = (page) => API.get(`/customer?page=${page}`);
export const getOne = (id) => API.get(`/customer/${id}`);

// Transfer API methods

export const addTransfer = (transfer) => API.post('/transfer/add', transfer);
export const getUserTransfer = (userId, page) => API.get(`/transfer/${userId}?page=${page}`);