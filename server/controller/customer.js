const mongoose = require("mongoose");

const Customer = require("./../models/customer");

const validateEmail = (email) => {
    const re =
        /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
};

function validatePhoneNumber(input_str) {
    var re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;

    return re.test(input_str);
}

const customerCtl = {
    add: async (req, res) => {
        const { name, email, balance, phone } = req.body;

        try {
            if (!name || !email || !balance || !phone)
                return res.status(400).json({message: "Please fill all fields."});

            if(!validateEmail(email))
                return res.status(400).json({message: "Invalid email."});

            if(!validatePhoneNumber(phone))
                return res.status(400).json({message: "Invalid phone number."});

            const oldCustomer = await Customer.findOne({email});

            if (oldCustomer)
                return res.status(400).json({message: "This email already exists"});

            const newCustomer = await Customer.create({name, email, balance, phone});
            res.status(200).json(newCustomer);
        } catch (error) {
            res.status(500).json({ message: error.message });
            console.log(error);
        }
    },
    getOne: async (req, res) => {
        const {id} = req.params;

        try {
            if (!mongoose.Types.ObjectId.isValid(id))
                return res.status(400).json({ message: "No Customer with this id" });
            
            const customer = await Customer.findById(id);
            res.status(200).json(customer);
        } catch (error) {
            res.status(500).json({message: error.message});
            console.error(error);
        }
    },
    getAll: async (req, res) => {
        const {page} = req.query;
        try {

            const LIMIT = 5;
            const startIndex = (Number(page) -1) * LIMIT;

            const total = await Customer.countDocuments({});
            const customers = await Customer.find().sort({_id: -1}).limit(LIMIT).skip(startIndex);

            res.status(200).json({data: customers, currentPage: Number(page), numOfPages: Math.ceil(total / LIMIT), total})
        } catch (error) {
            res.status(500).json({message: error.message});
            console.error(error);
        }
    },
}

module.exports = customerCtl;