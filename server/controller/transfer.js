const mongoose = require("mongoose");
const Transfer = require('../models/transfer');
const Customer = require('../models/customer');

const transferCtrl = {
    add: async (req, res) => {
        const {senderId, reciverId, amount} = req.body;

        try {
            if (reciverId === senderId) {
                return res.status(400).json({message: "You can't transfer money to yourself."});
            }

            if (!senderId || !reciverId || amount <= 0) 
                return res.status(400).json({message: "Please fill all fields."});

            const reciver = await Customer.findById(reciverId);
            const sender = await Customer.findById(senderId);

            if (!mongoose.Types.ObjectId.isValid(reciverId) || !reciver)
                return res.status(400).json({message: "Invalid resiver."});

            if (!mongoose.Types.ObjectId.isValid(senderId) || !sender || sender.balance < amount)
                return res.status(400).json({message: "Invalid sender."});

            if (typeof(amount) !== "number") 
                return res.status(400).json({message: "Invalid amount."})

            const newTransfer = await Transfer.create({senderId, reciverId, amount, reciverName: reciver.name, senderName: sender.name});

            const senderAfterTransfer = Object.assign(sender, {balance: sender.balance - amount});
            const reciverAfterTransfer = Object.assign(reciver, {balance: reciver.balance + amount});

            await Customer.findByIdAndUpdate(senderId, senderAfterTransfer, {new: true});
            await Customer.findByIdAndUpdate(reciverId, reciverAfterTransfer, {new: true});

            res.status(200).json(newTransfer);

        } catch (error) {
            res.status(500).json({message: error.message});
            console.error(error);
        }
    },
    getAll : async (req, res) => {
        const {page} = req.query;
        const {id} = req.params;

        try {
            const LIMIT = 5;
            const startIndex = (Number(page) -1) * LIMIT;
            const total = await Transfer.countDocuments({ $or: [ {senderId: id}, {reciverId: id} ]});

            const transfers = await Transfer.find({ $or: [ {senderId: id}, {reciverId: id} ]}).sort({_id: -1}).limit(LIMIT).skip(startIndex);

            res.status(200).json({data: transfers, currentPage: Number(page), numberOfPages: Math.ceil(total / LIMIT), total });
        } catch (error) {
            res.status(500).json({message: error.message});
            console.error(error);
        }
    }
};

module.exports = transferCtrl;