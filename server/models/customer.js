const mangoose = require("mongoose");

const customerSchema = new mangoose.Schema(
  {
    name: {
      type: String,
      trim: true,
      require: [true, "Please enter customer name"],
    },
    email: {
      type: String,
      trim: true,
      require: [true, "Please enter customer email"],
    },
    balance: {
      type: Number,
      default: 0,
    },
    phone: {
      type: String,
      trim: true,
      require: [true, "Please enter customer phone number"],
    }
  },
  {
    timestamps: true,
  }
);

module.exports = mangoose.model("Customer", customerSchema);
