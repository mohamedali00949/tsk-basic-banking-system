const mangoose = require("mongoose");

const transferScheme = new mangoose.Schema(
  {
    senderId: {
      type: String,
      trim: true,
      require: [true, "please enter sender"],
    },
    senderName: {
      type: String,
      trim: true,
      require: [true, "please enter sender name"],
    },
    reciverName: {
      type: String,
      trim: true,
      require: [true, "please enter reciver name"],
    },
    reciverId: {
      type: String,
      trim: true,
      require: [true, "please enter reciver"],
    },
    amount: {
      type: Number,
      require: [true, "please enter amount"],
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mangoose.model("Transfer", transferScheme);
