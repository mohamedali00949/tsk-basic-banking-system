const router = require('express').Router();
const transferCtrl = require('../controller/transfer');

router.get('/:id', transferCtrl.getAll);

router.post('/add', transferCtrl.add);

module.exports = router;