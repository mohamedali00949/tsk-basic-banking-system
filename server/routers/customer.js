const router = require('express').Router();
const CustomerController = require('./../controller/customer');

router.get('/', CustomerController.getAll);

router.get('/:id', CustomerController.getOne);

router.post('/add', CustomerController.add);

module.exports = router;