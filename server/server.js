const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path');
const mongoose = require('mongoose');
const dotenv = require('dotenv');

const app = express();

dotenv.config();

app.use(bodyParser.json({ limit: '30mb', extended: true }));
app.use(bodyParser.urlencoded({ limit: '30mb', extended: true }));
app.use(cors());

app.use('/customer', require('./routers/customer'));

app.use('/transfer', require('./routers/transfer'));

app.use(express.static(path.join(__dirname, 'build')));

app.get('*', function (req, res) {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

// Connect to MongoDB
// mongooDB cloud ====>> https://www.mongodb.com/cloud/atlas
const MANGOODB_UTL = process.env.MANGOODB_UTL;
const PORT = process.env.PORT || 5000; 

mongoose.connect(MANGOODB_UTL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
})
    .then(() => app.listen(PORT, () => console.log(`server Running at : http://localhost:${PORT}`) ))
    .catch((error) => console.log('The MangoDB Error is', error.message));